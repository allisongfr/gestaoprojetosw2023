---

Autor: Thiago Serra Ferreira de Carvalho
E-mail: thiago.carvalho (at) univag.edu.br
Data: 03.08.2023
TOCTitle: Cronograma

---

# Cronograma 
O cronograma é definido após a equipe de projeto ter clareza quanto ao escopo e requisitos.
Este artefato reflete a **previsão** para conclusão de cada etapa do projeto com base 
em tarefas associadas a cada uma.


No cronograma o sequenciamento de ativides é importante, pois diz quando uma tarefa poderá 
ser iniciada a partir da previsão de conclusão de uma tarefa anterior. A representação desse tipo
de relacionamento no tempo é feita normalmente usando-se um gráfico Gantt.


Junto a esta atividade você tem um exemplo de um gráfico de Gantt que poderá ser usado para confecção
do projeto que o grupo está propondo.


Acesso arquivo [do excel](03_Cronograma-Exemplo.xlsx) neste repositório.

## Lembrando!
- Cada etapa depende das tarefas que foram agrupadas em si. Ou seja, a etapa começa na data da tarefa 
que tem a menor data e, termina na da data da tarefa que tem maior data.
- Assim, as tarefas que defininem um marco (ou entregável) estão agrupadas numa mesma etapa.
- Logo, a previsão de conclusão do projeto é definida pela etapa que tem a maior data. 
- Um bom levantamento de requisitos é primordial para uma boa previsão inicial.



---
Ir para:
- [<< 02 - Termo de Abertura >>](02_TermoAbertura.md)
